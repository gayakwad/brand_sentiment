package sentimentanalysis.ws;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sentimentanalysis.common.Brand;

@Controller
@RequestMapping("/score")
public class AppController {

    @RequestMapping(value = "{entity}", method = RequestMethod.GET)
    public
    @ResponseBody
    Entity entityHandler(@PathVariable String entity) {
        Entity entityVO = new Entity();
        Brand brand = null;
        try {
            brand = Brand.valueOf(entity.toUpperCase());
        } catch (Exception ignored) {

        }
        if (brand == null) {
            entityVO.setEntity(entity + " entity not available");
            return entityVO;
        }else {
           entityVO = DataCache.get(brand);
        }
        return  entityVO;
    }
}