package sentimentanalysis.ws;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 02/11/12
 * Time: 5:30 PM
 */
public class Tag {
    String name;
    Float count;

    public Tag(String name, Float count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getCount() {
        return count;
    }

    public void setCount(Float count) {
        this.count = count;
    }
}
