package sentimentanalysis.common;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 21/10/12
 * Time: 5:24 PM
 */
public class SinceIdCache {

    private static ConcurrentHashMap<Brand, String> sinceIdMap = new ConcurrentHashMap<Brand, String>();

    public static void put(Brand brand, String sinceId) {
        sinceIdMap.put(brand, sinceId);
    }

    public static String get(Brand brand) {
        return sinceIdMap.get(brand);
    }
}

