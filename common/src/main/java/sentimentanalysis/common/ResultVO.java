package sentimentanalysis.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 21/10/12
 * Time: 6:57 PM
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultVO {
    String max_id_str;
    String refresh_url;
    List<TweetVO> results;

    public String getMax_id_str() {
        return max_id_str;
    }

    public void setMax_id_str(String max_id_str) {
        this.max_id_str = max_id_str;
    }

    public String getRefresh_url() {
        return refresh_url;
    }

    public void setRefresh_url(String refresh_url) {
        this.refresh_url = refresh_url;
    }

    public List<TweetVO> getResults() {
        return results;
    }

    public void setResults(List<TweetVO> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ResultVO{" +
                "max_id_str='" + max_id_str + '\'' +
                ", refresh_url='" + refresh_url + '\'' +
                ", results=" + results +
                '}';
    }
}
