package sentimentanalysis.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 22/10/12
 * Time: 12:11 AM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetVO {
    String id_str;
    String created_at;
    String from_user;
    String from_user_id_str;
    //String geo;
    String iso_language_code;
    String text;
    Double score;
    String brand;


    public String getId_str() {
        return id_str;
    }

    public void setId_str(String id_str) {
        this.id_str = id_str;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getFrom_user() {
        return from_user;
    }

    public void setFrom_user(String from_user) {
        this.from_user = from_user;
    }

    public String getFrom_user_id_str() {
        return from_user_id_str;
    }

    public void setFrom_user_id_str(String from_user_id_str) {
        this.from_user_id_str = from_user_id_str;
    }

    /*public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }*/

    public String getIso_language_code() {
        return iso_language_code;
    }

    public void setIso_language_code(String iso_language_code) {
        this.iso_language_code = iso_language_code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "TweetVO{" +
                "id_str='" + id_str + '\'' +
                ", created_at='" + created_at + '\'' +
                ", from_user='" + from_user + '\'' +
                ", from_user_id_str='" + from_user_id_str + '\'' +
                ", iso_language_code='" + iso_language_code + '\'' +
                ", text='" + text + '\'' +
                ", sore='" + score + '\'' +
                '}';
    }
}
