package sentimentanalysis.analysis.basic;


import sentimentanalysis.analysis.SentimentCalculator;
import sentimentanalysis.common.TweetVO;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 23/10/12
 * Time: 2:50 PM
 */
public class BasicSentimentCalculator implements SentimentCalculator {
    private static ConcurrentHashMap<String, Float> map = new ConcurrentHashMap<String, Float>();

    static {
        putInMap("sentiwords.txt");
        putInMap("sentislang.txt");
    }

    private static void putInMap(String fileName) {
        try {
            InputStream is;
            is = BasicSentimentCalculator.class.getClassLoader().getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] split = strLine.split("\t");
                map.put(split[1], Float.valueOf(split[0]));
            }
            is.close();
        } catch (Exception e) {
            Logger.getLogger(BasicSentimentCalculator.class.getName()).log(Level.SEVERE, null, e);
        }
        Logger.getLogger(BasicSentimentCalculator.class.getName()).log(Level.INFO, "loaded " + map.size() + " keywords form filename " + fileName);
    }


    @Override
    public Double get(String entity, String text) {
        Double result = 0d;
        if (null == text) return result;
        StringTokenizer tokenizer = new StringTokenizer(text);
        while (tokenizer.hasMoreElements()) {
            Float aFloat = map.get(tokenizer.nextToken());
            if (aFloat != null)
                result += aFloat;
        }
        return result;
    }


    @Override
    public void populateScore(String entity, List<TweetVO> results) {
        if (null != entity && (null != results && results.size() > 0)) {
            for (TweetVO result : results) {
                result.setScore(get(entity, result.getText()));
            }
        }
    }
}
