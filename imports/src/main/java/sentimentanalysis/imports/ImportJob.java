package sentimentanalysis.imports;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import sentimentanalysis.analysis.custom.GaussianSentimentCalculator;
import sentimentanalysis.common.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 20/10/12
 * Time: 9:48 PM
 */
public class ImportJob implements Job {


    public void execute(JobExecutionContext context) throws JobExecutionException {
        Brand brand = (Brand) context.getJobDetail().getJobDataMap().get("brand");
        List<TweetVO> results = getTweets(brand);
        if (null != results && results.size() > 0) {
            try {
                new GaussianSentimentCalculator().populateScore(brand.name(), results);
            } catch (Exception e) {
                throw new JobExecutionException(e);
            }
            storeTweets(brand, results);
        }
    }

    private List<TweetVO> getTweets(Brand brand) {
        ResultVO resultVO = null;
        HttpClient httpClient = HttpUtil.getHttpClient();
        try {
            HttpGet httpget = new HttpGet(brand.getTwitterURL());
            Logger.getLogger(ImportJob.class.getName()).log(Level.INFO, "executing request " + httpget.getURI());

            HttpResponse response = httpClient.execute(httpget);
            HttpEntity entity = response.getEntity();
            String content = EntityUtils.toString(entity);
            EntityUtils.consume(entity);

            ObjectMapper mapper = new ObjectMapper();
            resultVO = mapper.readValue(content, ResultVO.class);

            Logger.getLogger(ImportJob.class.getName()).log(Level.INFO,
                    "total no of tweets fetched for " + brand.name() +
                            ": " + ((resultVO.getResults() == null) ? 0 : resultVO.getResults().size()));

            SinceIdCache.put(brand, resultVO.getMax_id_str());

        } catch (IOException e) {
            Logger.getLogger(ImportJob.class.getName()).log(Level.SEVERE, null, e);
        } catch (URISyntaxException e) {
            Logger.getLogger(ImportJob.class.getName()).log(Level.SEVERE, null, e);
        }

        return resultVO == null ? null : resultVO.getResults();
    }

    private void storeTweets(Brand brand, List<TweetVO> tweets) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = DBUtil.getConnection();
            statement = connection.prepareStatement("insert into `brand_tweets` ( `id`, `text`, " +
                    "`iso_language`, `creation_date`, `user_name`, `geo`, `brand`, `score`) " +
                    "values (?,?,?,?,?,?,?,?)");

            for (TweetVO tweet : tweets) {
                statement.setString(1, tweet.getId_str());
                statement.setString(2, tweet.getText());
                statement.setString(3, tweet.getIso_language_code());
                statement.setTimestamp(4, getTwitterDate(tweet.getCreated_at()));
                statement.setString(5, tweet.getFrom_user());
                statement.setString(6, null);
                statement.setString(7, brand.name());
                statement.setDouble(8, tweet.getScore());
                statement.addBatch();
            }
            int[] ints = statement.executeBatch();
            Logger.getLogger(ImportJob.class.getName()).log(Level.WARNING, Arrays.toString(ints));
        } catch (SQLException e) {
            Logger.getLogger(ImportJob.class.getName()).log(Level.SEVERE, e.getMessage());
        } finally {
            DBUtil.closeQuietly(statement);
            DBUtil.closeQuietly(connection);
        }
    }


    private Timestamp getTwitterDate(String date) {
        if (date == null) return null;
        final String TWITTER = "EEE, dd MMM yyyy HH:mm:ss ZZZZZ"; // Mon, 22 Oct 2012 08:10:58 +0000
        SimpleDateFormat sf = new SimpleDateFormat(TWITTER);
        sf.setLenient(true);
        Timestamp timestamp = null;
        try {
            timestamp = new Timestamp(sf.parse(date).getTime());
        } catch (ParseException e) {
            Logger.getLogger(ImportJob.class.getName()).log(Level.SEVERE, null, e);
        }
        return timestamp;
    }


}
