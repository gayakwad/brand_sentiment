package sentimentanalysis.imports;

import sentimentanalysis.analysis.custom.GaussianSentimentCalculator;
import sentimentanalysis.common.TweetVO;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: karthik
 * Date: 21/11/12
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class AccuracyTest {
    private static final String TEST_TWEETS_CSV = "tweet_data.csv";



    public static void main(String[] args) {

        List<TweetVO> tweets=new ArrayList<TweetVO>();
        List<Integer> expectedScores=new ArrayList<Integer>();
        try {
            System.out.println("loading file >>>>>>>>>>>> " + TEST_TWEETS_CSV);
            InputStream is;
            is = AccuracyTest.class.getClassLoader().getResourceAsStream(TEST_TWEETS_CSV);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String strLine;

            while ((strLine = br.readLine()) != null) {
                //"apple",1,"126415614616154112","Tue Oct 18 21:53:25 +0000 2011","Now all @Apple has to do is get swype on the iphone and it will be crack. Iphone that is"
                String[] split = strLine.split(",");
                String[] split2 = strLine.split("\",\"");
                String brand=split[0];
                int score=Integer.parseInt(split[1]);
                String text=split2[2];
                TweetVO tweet = new TweetVO();
                tweet.setBrand(brand);
                tweet.setText(text);
                tweets.add(tweet);
                expectedScores.add(new Integer(score));
            }
            is.close();

            for (TweetVO tweet : tweets) {
                new GaussianSentimentCalculator().populateScore(tweet.getBrand(), Arrays.asList(tweet));
            }

            double correct=0;
            double wrong=0;

            for (TweetVO tweet : tweets) {
                int index = tweets.indexOf(tweet);
                if(tweet.getScore()>0.0 && expectedScores.get(index).intValue()==1 || tweet.getScore()<-0.0 && expectedScores.get(index).intValue()==0){
                    correct++;
                }
                else {
                    System.out.println(tweet.getScore()+" "+tweet.getText());
                    wrong++;
                }
            }
            System.out.println("Accuracy = "+(correct/(correct+wrong))*100.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
