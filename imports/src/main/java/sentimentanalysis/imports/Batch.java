package sentimentanalysis.imports;

import sentimentanalysis.analysis.custom.GaussianSentimentCalculator;
import sentimentanalysis.common.DBUtil;
import sentimentanalysis.common.TweetVO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: abhisheg
 * Date: 31/10/12
 * Time: 4:24 PM
 */
public class Batch {

    public static final String FIRST = "select * from brand_tweets limit 1000";
    public static final String NEXT = "select * from brand_tweets where id > ? limit 10000";
    private static Long cur_id;
    private static Long count = 0l;
    public static final String UPDATE_SQL = "UPDATE brand_tweets SET score=? WHERE id=?";


    public static void main(String[] args) throws Exception {
        while (true) {
            long start1 = System.currentTimeMillis();
            Connection connection = DBUtil.getConnection();
            PreparedStatement statement = connection.prepareStatement(cur_id == null ? FIRST : NEXT);
            PreparedStatement updateState = connection.prepareStatement(UPDATE_SQL);
            if (cur_id != null) {
                statement.setLong(1, cur_id);
            }
            ResultSet rs = statement.executeQuery();
            if (!rs.next()) {
                return;
            }
            do {
                TweetVO t = new TweetVO();
                t.setBrand(rs.getString("brand"));
                t.setText(rs.getString("text"));
                cur_id = rs.getLong("id");
                count++;
                new GaussianSentimentCalculator().populateScore(t.getBrand(), Arrays.asList(t));
                updateState.setLong(2, cur_id);
                updateState.setDouble(1, t.getScore());
                updateState.execute();

            } while (rs.next());

            //updateState.executeBatch();
            // updateState.clearBatch();
            System.out.println(">>>>>>>>" + (System.currentTimeMillis() - start1));
            System.out.println(count);

        }
    }
}
