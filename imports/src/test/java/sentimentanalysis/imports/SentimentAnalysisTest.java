package sentimentanalysis.imports;

import org.junit.Test;
import sentimentanalysis.analysis.custom.GaussianSentimentCalculator;
import sentimentanalysis.common.TweetVO;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 24/10/12
 * Time: 2:44 PM
 */
public class SentimentAnalysisTest {

    @Test
    public void testSentiment() {
        TweetVO lt = new TweetVO();
        lt.setText("@apple, #siri can't connect to the network -- it's less useful that voicecontrol now... :(");
        LinkedList<TweetVO> l = new LinkedList<TweetVO>();
        l.add(lt);
        try {
            new GaussianSentimentCalculator().populateScore("apple", l);
            System.out.println(lt.getScore()+"==");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
